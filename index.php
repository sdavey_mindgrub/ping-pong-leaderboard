<?php
require __DIR__.'/functions.php';

$ships = get_ships();

$errorMessage = '';
if (isset($_GET['error'])) {
    switch ($_GET['error']) {
        case 'missing_data':
            $errorMessage = 'Don\'t forget to select some ships to battle!';
            break;
        case 'bad_ships':
            $errorMessage = 'You\'re trying to fight with a ship that\'s unknown to the galaxy?';
            break;
        case 'bad_quantities':
            $errorMessage = 'You pick strange numbers of ships to battle - try again.';
            break;
        default:
            $errorMessage = 'There was a disturbance in the force. Try again.';
    }
}

$Aplayer1score = isset($_POST['Aplayer1score']) ? $_POST['Aplayer1score'] : null;
$Aplayer2score = isset($_POST['Aplayer2score']) ? $_POST['Aplayer2score'] : null;
$Aplayer3score = isset($_POST['Aplayer3score']) ? $_POST['Aplayer3score'] : null;
$Bplayer1score = isset($_POST['Bplayer1score']) ? $_POST['Bplayer1score'] : null;
$Bplayer2score = isset($_POST['Bplayer2score']) ? $_POST['Bplayer2score'] : null;
$Bplayer3score = isset($_POST['Bplayer3score']) ? $_POST['Bplayer3score'] : null;

$outcome = battle($Aplayer1score, $Aplayer2score, $Aplayer3score, $Bplayer1score, $Bplayer2score, $Bplayer3score);

?>

<html>
    <head>
        <meta charset="utf-8">
           <meta http-equiv="X-UA-Compatible" content="IE=edge">
           <meta name="viewport" content="width=device-width, initial-scale=1">
           <title>Ping Pong Leader Board</title>

           <!-- Bootstrap -->
           <link href="css/bootstrap.min.css" rel="stylesheet">
           <link href="css/style.css" rel="stylesheet">
           <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

           <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
           <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
           <!--[if lt IE 9]>
             <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
             <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
           <![endif]-->
    </head>

    <!-- <?php if ($errorMessage): ?>
        <div>
            <?php echo $errorMessage; ?>
        </div>
    <?php endif; ?> -->

    <body>
        <div class="container">
            <div class="page-header">
                <h1>Standings</h1>
            </div>
            <table class="table table-hover">
                <tbody>
                    <?php foreach ($ships as $ship): ?>
                            <tr>
                                <td><?php echo $ship->getRank().". "; ?><?php echo $ship->getName(); ?></td>
                                <?php if($ship->getRank() < $ships['four']->getRank()): ?>
                                    <td>
                                        <form method="POST" action="/battle.php">
                                            <input type="submit" name="letter" value="<?php echo $ship->getName(); ?>"/>
                                        </form>
                                    </td>
                                <?php endif; ?>
                            </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </body>







</html>
