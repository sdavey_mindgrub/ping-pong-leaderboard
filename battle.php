<?php
require __DIR__.'/functions.php';

$Bplayer3score = isset($_POST['letter']) ? $_POST['letter'] : null;
var_dump($Bplayer3score);

?>

<html>
    <head>
        <meta charset="utf-8">
           <meta http-equiv="X-UA-Compatible" content="IE=edge">
           <meta name="viewport" content="width=device-width, initial-scale=1">
           <title>Challenge</title>

           <!-- Bootstrap -->
           <link href="css/bootstrap.min.css" rel="stylesheet">
           <link href="css/style.css" rel="stylesheet">
           <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
           <link href='http://fonts.googleapis.com/css?family=Audiowide' rel='stylesheet' type='text/css'>
           
           <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
           <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
           <!--[if lt IE 9]>
             <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
             <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
           <![endif]-->
    </head>
    <body>

        <div class="container">
            <div>
                <div class="battle-box center-block border">
                <div>
                    <form method="POST" action="/index.php">
                        <h2 class="text-center">Enter scores</h2>
                        <p class="text-center">Game 1</p>
                        <select class="center-block form-control btn drp-dwn-width btn-default btn-lg dropdown-toggle" name="Aplayer1score">
                            <option value="">Enter Player 1 Scores</option>
                            <?php for ($i = 1; $i <= 30; $i++): ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                            <?php endfor; ?>
                        </select>
                        <select class="center-block form-control btn drp-dwn-width btn-default btn-lg dropdown-toggle" name="Bplayer1score">
                            <option value="">Enter Player 2 Scores</option>
                            <?php for ($i = 1; $i <= 30; $i++): ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                            <?php endfor; ?>
                        </select>
                        <br>
                        <p class="text-center">Game 2</p>
                        <select class="center-block form-control btn drp-dwn-width btn-default btn-lg dropdown-toggle" name="Aplayer2score">
                            <option value="">Enter Player 1 Scores</option>
                            <?php for ($i = 1; $i <= 30; $i++): ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                            <?php endfor; ?>
                        </select>
                        <select class="center-block form-control btn drp-dwn-width btn-default btn-lg dropdown-toggle" name="Bplayer2score">
                            <option value="">Enter Player 2 Scores</option>
                            <?php for ($i = 1; $i <= 30; $i++): ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                            <?php endfor; ?>
                        </select>
                        <br>
                        <p class="text-center">Game 3 (if necessary)</p>
                        <select class="center-block form-control btn drp-dwn-width btn-default btn-lg dropdown-toggle" name="Aplayer3score">
                            <option value="">Enter Player 1 Scores</option>
                            <?php for ($i = 1; $i <= 30; $i++): ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                            <?php endfor; ?>
                        </select>
                        <select class="center-block form-control btn drp-dwn-width btn-default btn-lg dropdown-toggle" name="Bplayer3score">
                            <option value="">Enter Player 2 Scores</option>
                            <?php for ($i = 1; $i <= 30; $i++): ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                            <?php endfor; ?>
                        </select>
                        <br>
                        <button class="btn btn-md btn-danger center-block" type="submit">Submit</button>
                    </form>
                </div>
            </div>
                <!-- <h2 class="text-center">The Matchup:</h2>
                <p class="text-center">
                    <br>
                    <?php echo $ship1Quantity; ?> <?php echo $ship1->getName(); ?><?php echo $ship1Quantity > 1 ? 's': ''; ?>
                    VS.
                    <?php echo $ship2Quantity; ?> <?php echo $ship2->getName(); ?><?php echo $ship2Quantity > 1 ? 's': ''; ?>
                </p>
            </div>
            <div class="result-box center-block">
                <h3 class="text-center audiowide">
                    Winner:
                    <?php if ($outcome['winning_ship']): ?>
                        <?php echo $outcome['winning_ship']->getName(); ?>
                    <?php else: ?>
                        Nobody
                    <?php endif; ?>
                </h3>
                <p class="text-center">
                    <?php if ($outcome['winning_ship'] == null): ?>
                        Both ships destroyed each other in an epic battle to the end.
                    <?php else: ?>
                        The <?php echo $outcome['winning_ship']->getName(); ?>
                        <?php if ($outcome['used_jedi_powers']): ?>
                            used its Jedi Powers for a stunning victory!
                        <?php else: ?>
                            overpowered and destroyed the <?php echo $outcome['losing_ship']->getName() ?>s
                        <?php endif; ?>
                    <?php endif; ?>
                </p>
            </div> -->
            <a href="/index.php"><p class="text-center"><i class="fa fa-undo"></i> Battle again</p></a>
        
            <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
            <!-- Include all compiled plugins (below), or include individual files as needed -->
            <script src="js/bootstrap.min.js"></script>
        </div>
    </body>
</html>
