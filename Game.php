<?php
require __DIR__.'/functions.php';

class Game {

	//game 1
	private $Aplayer1score;

	private $Bplayer1score;

	//game 2
	private $Aplayer2score;

	private $Bplayer2score;

	//game 3
	private $Aplayer3score;

	private $Bplayer3score;

	public function getPlayerATotal() {
		return $this->Aplayer1score + $this->Aplayer2score + $this->Aplayer3score;
	}

	public function getPlayerBTotal() {
		return $this->Bplayer1score + $this->Bplayer2score + $this->Bplayer3score;
	}

	public function setAplayer1Score($Aplayer1score) {
		return $this->Aplayer1score = $Aplayer1score;
	}




}