<?php

require_once __DIR__.'/lib/Ship.php';

function get_ships()
{
    $ships = array();

    $ship = new Ship('Player A');
    $ship->setRank(1);
    $ships['one'] = $ship;

    $ship2 = new Ship('Player B');
    $ship2->setRank(2);
    $ships['two'] = $ship2;

    $ship3 = new Ship('Player C');
    $ship3->setRank(3);
    $ships['three'] = $ship3;

    $ship4 = new Ship('Player D');
    $ship4->setRank(4);
    $ships['four'] = $ship4;
    

    return $ships;
}

/**
 * Our complex fighting algorithm!
 *
 * @return char A or B for player 1 or 2
 */
function battle($a1, $a2, $a3, $b1, $b2, $b3)
{   
    $scoreA = $a1 + $a2 + $a3;
    $scoreB = $b1 + $b2 + $b3;
    if ($scoreA > $scoreB) {
        return 'A';
    }
    return 'B';
}
